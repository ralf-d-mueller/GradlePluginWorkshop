= Writing Gradle Plugins in Groovy : Integration Testing
Schalk W. Cronjé <ysb33r@gmail.com>

Gradle provide a testing API, called `TestKit`. It provides the ability to run plugins as if they would be loaded in the build. This can be used with unit tests, but many times these tests can be longer running than which is acceptable for unit tests. They might also make use of networking resources, which once again, takes them out the scope of unit testing. It is therefore worthwhile exploring adding an integration test suite utilising `TestKit` to your build.

== Add an integration testing source set

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=integration-test-sourceset]
----
<1> Create two configurations and for convention name them in a similar way to `testXXX` configurations - `integrationTestCompile` and `integrationTestRuntime`.
<2> Extending the `testCompile` configuration is optional. It depends on whether you require the dependencies for `testCompile` to also be on the classpath when the integration tests are run.
<3> `integrationTestRuntime` has to extend `integrationTestCompile` otherwise the required dependencies will not be available at runtime. Extending `testRuntime` is optional and will depend on your specific plugin's context.
<4> `sourceSets` are made availabe via the `java` plugin. When applying the `groovy` plugin the base is automatically applied. Source sets form the core of the source convention for JVM languages.
<5> Each source set requires a name which in turn will affect the name of taks that are created. For convention you should use `integrationTest`.
<6> Regardless of whether you are writing integration tests in Javaor not, you should always configure this.
<7> By convention Groovy source code should be placed in `src/NAME-OF-SOURCE-SET/groovy` and in this case will be `src/integrationTest/groovy`.
<8> The classpath for compilation is made up of the classes for your plugin plus all dependencies that you specify for the `integrationTestCompile` configuration.
<9> The runtime classpath will be made up of the integration test classes, the comilation classpath and all dependencies specified for the `integrationTestRuntime` configuration. (`output` is a property resolved off the source set.)

Run `./gradlew tasks --all` and look at the output.

[listing]
----
Build tasks
-----------
...
integrationTestClasses - Assembles integration test classes.
...

Other tasks
-----------
...
compileIntegrationTestGroovy - Compiles the integrationTest Groovy source.
compileIntegrationTestJava - Compiles integration test Java source.
...
processIntegrationTestResources - Processes integration test resources.
...
----

Note how these compilation tasks were added.

== Configuring the integration task

Before runnign integration tests, a task needs to be added. As this is a JVM-based test suite with Spock Framework, the task type with be {gradle-api}/org/gradle/api/tasks/testing/Test.html[Test]. Add the following to your `build.gradle` file.

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=integration-testing]
----

Now run the integration test

[listing]
----
./gradlew integrationTest

:compileJava NO-SOURCE
:compileGroovy UP-TO-DATE
:processResources UP-TO-DATE
:classes UP-TO-DATE
:compileIntegrationTestJava NO-SOURCE
:compileIntegrationTestGroovy NO-SOURCE
:processIntegrationTestResources NO-SOURCE
:integrationTestClasses UP-TO-DATE
:jar UP-TO-DATE
:integrationTest NO-SOURCE

BUILD SUCCESSFUL
----

This indicates that your integration test configuration is set up correctly. `NO-SOURCE` is reported as you have not added any tests yet.

== Adding Testkit to configuration

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=integration-test-with-testkit]
----
<1> `gradleTestKit()` makes the appropriate testing API available on the classpath.

NOTE: Certain versions of Gradle has a bug in resolving the classpath when `gradleTestKit()` is used. Always ensure that when you use it with `localGroovy()` and `gradleApi()` that it is the last one of the three being added to the `integrationTestCompile` or `testCompile` configuration.

== Add an integration test

Start my creating the package folder underneath the folder that you have previously configure in the source set.

[listing]
----
mkdir -p src/integrationTest/groovy/pluginworkshop
----

Create a new Spock test called `FileManifestIntegrationSpec.groovy` in the package folder.
This test will be a varition of the earlier *FileManifest must list each file with absolute path and size* unit test.

.src/integrationTest/pluginworkshop/FileManifestIntegrationSpec.groovy
[source,groovy]
----
include::{integrationdir}/FileManifestIntegrationSpec.groovy[tags=initial-setup]
----
<1> Convention is to end the tests with `IntegrationSpec`.
<2> When using `TestKit`, creating a temporary folder with a JUnit rule is useful test cleanup.

.src/integrationTest/pluginworkshop/FileManifestIntegrationSpec.groovy
[source,groovy]
----
include::{integrationdir}/FileManifestIntegrationSpec.groovy[tags=test-example]
----
<1> A possibly dirty, but quick way to generate a build script is to put it all in a string and dump it to the file.
<2> {gradle-api}/org/gradle/testkit/runner/GradleRunner.html[GradleRunner] is the core of `TestKit`. It will be worth your while to understand all of the methods that are available to configure your build.
<3> Setup the project to execute within the project directory defined earlier
<4> These are the same kind of arguments that will be passed to Gradle on the command-line. As this test will exercise the `fileManifest` task this is what will be passed.
<5> Use of `forwardOutput` will cause the execution output to appear in the test report. This can be quite usefull when passing `-i` or `-s` as the command-line parameters to obtain additional debugging output.
<6> In latter versions of Gradle the `TestKit` API allows for checking specific outcomes. In this the test is to see whether the `fileManifest` task executed correctly.
<7> It is completely possible to do additional tests outside of the `GradleRunner` instance. Here a simple verification that the manifest file actually git created and that the execution did not just report `SUCCESS`.

Run the integration test.

[listing]
----
./gradlew integrationTest

:compileJava NO-SOURCE
:compileGroovy UP-TO-DATE
:processResources UP-TO-DATE
:classes UP-TO-DATE
:compileIntegrationTestJava NO-SOURCE
:compileIntegrationTestGroovy
:processIntegrationTestResources NO-SOURCE
:integrationTestClasses
:jar UP-TO-DATE
:integrationTest

pluginworkshop.FileManifestIntegrationSpec > FileManifest must list each file with absolute path and size FAILED
    org.gradle.testkit.runner.UnexpectedBuildFailure at FileManifestIntegrationSpec.groovy:62

1 test completed, 1 failed
:integrationTest FAILED
----

Open the test report which is at `build/reports/integrationTests/index.html`.

image::integration-test-failure.png[]

The failure is caused by the plugin not being on the classpath. Due to the way that `GradleRunner` runs the test in a separate JVM what you might have thought to be on the classpath is not. To work around the problem insert the following line of code (hardcoded path for now) into your test just before running `GradleRunner`.

.src/integrationTest/pluginworkshop/FileManifestIntegrationSpec.groovy
[source,groovy]
----
include::{integrationdir}/FileManifestIntegrationSpec.groovy[tags=pluginclasspath]
----

This kind of approach does not scale and is one of the issues with just using `TestKit` as-is. In it's simplest form the build script can be used to generate a classpath text file which can then be read by the script again. However, you should investigate  https://docs.gradle.org/current/userguide/javaGradle_plugin.html[Java Gradle Plugin Development plugin], https://github.com/ajoberstar/gradle-stutter[Stutter] or https://plugins.gradle.org/plugin/org.ysb33r.gradletest[GradleTest] before making such a decision.

== Adding a classpath file

If you decide that a simple classpath text file will suit your needs, modify your `build.gradle` file as follows:

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=integration-test-classpath]
----
<1> Re-run the manigest everytime the runtime classpath changes.
<2> Don't run this if the integration tests have not been created - create them first.
<3> Ensure the manifest is created before the integration tests are run.
<4> Pass the location as a system property. Even-though the test can have a default path for use when in an IDE, doing this allows CI to overwrite the build directory and the tests will know where the new directory is located.


Set a class property for the classpath manifest file in your test class.

.src/integrationTest/pluginworkshop/FileManifestIntegrationSpec.groovy
[source,groovy]
----
include::{integrationdir}/FileManifestIntegrationSpec.groovy[tags=static-properties]
----


Now modify your `when` block to read the file.


.src/integrationTest/pluginworkshop/FileManifestIntegrationSpec.groovy
[source,groovy]
----
include::{integrationdir}/FileManifestIntegrationSpec.groovy[tags=with-plugin-classpath-file,indent=0]
----


'''

*Next*: link:compatibility-testing.html[Compatibility Testing]